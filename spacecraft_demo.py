import time
from datetime import datetime

import numpy as np
from matplotlib import pyplot as plt
import pickle

from SpacecraftEnvironment import SpacecraftEnvironment
from GMPHD_filter import GMPHD_filter_scEnv

if __name__ == '__main__':
    # Create the environment
    load_data = False # either load data or generate new data log
    env = SpacecraftEnvironment()
    env.verbose = False
    clutter = True

    # Uncomment if you want spacecraft model for the target
    ####################################################################################
    env.load_object()
    ####################################################################################

    x_0 = np.array([[-600.0],[0.0], [0.0], [0.0], [1.2717456], [0.0], [0.0], [0.0], [0.0]])
    x_0[6:,0] = env.dcm_to_mrp(env.compute_dcm(x_0))[:,0]

    t_vec = np.arange(0, 2*60*60, env.dt)
    t_vec = np.arange(0, 60*60, env.dt)

    # Compute ground truth and measurements
    if load_data:
        with open("datalog.pkl", "rb") as f:
            data_log = pickle.load(f)
            X_k = data_log.get("X_k")
            Y_k = data_log.get("Y_k")
    else:
        X_k, Y_k = env.ground_truth(x_0, t_vec, clutter)
        data_log = {"X_k": X_k, "Y_k": Y_k}
        with open("datalog.pkl", "wb") as f:
            pickle.dump(data_log, f)

    # for idx, element in enumerate(Y_k):
    #     print(idx, element)

    fig, ax = env.visualize(X_k)
    plt.savefig("../../Figures/periodic_orbit_non_spherical.pdf", bbox_inches='tight', dpi=300)

    # fig1, fig2, fig3, fig4 = env.plot_ground_truth(X_k, Y_k, t_vec)

    initial_state = X_k[:,0]
    N_particles = 50
    sc_nav_filter = GMPHD_filter_scEnv(env, initial_state, birthCovariance=np.array([[10., 0, 0], [0, 10., 0], [0, 0, 10.]]),
                                       birthWeight=0.1, pS_inView=0.99, pS_outView=0.01, pD=0.99, Q=None, R=None,
                                       num_particles=N_particles, max_features=10)
    state_error = []
    map_error = []
    sigma_x = []
    sigma_y = []
    sigma_z = []
    sigma_vx = []
    sigma_vy = []
    sigma_vz = []
    num_meas = []

    particle_error = np.zeros([N_particles,len(t_vec),len(x_0)])
    particle_weight = np.zeros([N_particles,len(t_vec)])
    Neff = np.zeros(len(t_vec))
    for i, y_k in enumerate(Y_k[1:]):  # don't use measurements from initial position; start at tstep 1
        t0 = time.time()
        sc_nav_filter.filter(y_k)
        t1 = time.time()
        print("Tstep=%d in %.2f seconds" % (i + 1, t1 - t0))

        # particles_tk = sc_nav_filter.particles
        # for j, particle_j in enumerate(particles_tk):
        #     particle_weight[j,i] = particle_j.get("Particle Weight")
        #     for k in range(len(x_0)):
        #         particle_error[j,i,k] = X_k[k, i + 1] - particle_j.get("Pose State")[k]

        Neff[i] = sc_nav_filter.Neff
        state_est, map_est, state_cov = sc_nav_filter.get_poseState()
        state_error.append(X_k[:, i + 1] - state_est)
        map_error.append(X_k[:, i + 1] - map_est)
        sigma_x.append(np.sqrt(state_cov[0, 0]))
        sigma_y.append(np.sqrt(state_cov[1, 1]))
        sigma_z.append(np.sqrt(state_cov[2, 2]))
        sigma_vx.append(np.sqrt(state_cov[3, 3]))
        sigma_vy.append(np.sqrt(state_cov[4, 4]))
        sigma_vz.append(np.sqrt(state_cov[5, 5]))
        num_meas.append(len(y_k))

        err_bound = np.array([sigma_x[i], sigma_y[i], sigma_z[i], sigma_vx[i], sigma_vy[i], sigma_vz[i]])
        if any(np.abs(state_error[i][:6]) > 2*err_bound):
            print("State error exceeds 2-sigma bound!")

    state_err = np.array(state_error)
    map_err = np.array(map_error)
    sigma_x = np.array(sigma_x)
    sigma_y = np.array(sigma_y)
    sigma_z = np.array(sigma_z)
    sigma_vx = np.array(sigma_vx)
    sigma_vy = np.array(sigma_vy)
    sigma_vz = np.array(sigma_vz)
    filter_log = {"t_vec": t_vec, "state_error":state_err, "sigma_x": sigma_x, "sigma_y": sigma_y, "sigma_z": sigma_z,
                  "sigma_vx": sigma_vx, "sigma_vy": sigma_vy, "sigma_vz": sigma_vz, "num_meas": num_meas,
                  "Neff": Neff}
                  # "particle_error": particle_error, "particle_weight": particle_weight}
    fname = "filter_log_" + str(datetime.now().strftime("%d-%m-%Y_%H-%M-%S")) + ".pkl"
    with open(fname, "wb") as f:
        pickle.dump(filter_log, f)

    fig, axs = plt.subplots(2,2,figsize=(10,5))
    fig.subplots_adjust(wspace=0.3)
    axs[0,0].plot(t_vec, state_err[:,0], 'r', label="MMSE X Error")
    axs[0,0].plot(t_vec, map_err[:,0], 'g', label="MAP X Error")
    axs[0,0].plot(t_vec, 2*sigma_x, '--k')
    axs[0,0].plot(t_vec, -2*sigma_x, '--k', label=r'2$\sigma$')
    # axs[0,0].set_xlabel('Sim Time [sec]')
    axs[0,0].set_ylabel('Position Error [m]')
    axs[0,0].legend()
    axs[1,0].plot(t_vec, state_err[:, 1], 'r', label="MMSE Y Error")
    axs[1,0].plot(t_vec, map_err[:,1], 'g', label="MAP Y Error")
    axs[1,0].plot(t_vec, 2*sigma_y, '--k')
    axs[1,0].plot(t_vec, -2*sigma_y, '--k', label=r'2$\sigma$')
    axs[1,0].set_xlabel('Sim Time [sec]')
    axs[1,0].set_ylabel('Position Error [m]')
    axs[1,0].legend()
    axs[0,1].plot(t_vec, state_err[:, 3], 'r', label="MMSE X Error")
    axs[0,1].plot(t_vec, map_err[:,3], 'g', label="MAP X Error")
    axs[0,1].plot(t_vec, 2 * sigma_vx, '--k')
    axs[0,1].plot(t_vec, -2 * sigma_vx, '--k', label=r'2$\sigma$')
    # axs[0,1].set_xlabel('Sim Time [sec]')
    axs[0,1].set_ylabel('Velocity Error [m/s]')
    axs[0,1].legend()
    axs[1,1].plot(t_vec, state_err[:, 4], 'r', label="MMSE Y Error")
    axs[1,1].plot(t_vec, map_err[:,4], 'g', label="MAP Y Error")
    axs[1,1].plot(t_vec, 2 * sigma_vy, '--k')
    axs[1,1].plot(t_vec, -2 * sigma_vy, '--k', label=r'2$\sigma$')
    axs[1,1].set_xlabel('Sim Time [sec]')
    axs[1,1].set_ylabel('Velocity Error [m/s]')
    axs[1,1].legend()
    plt.savefig("../../Figures/spacecraft_error_covar_clutter.pdf", bbox_inches='tight', dpi=300)

    plt.figure()
    plt.plot(t_vec, num_meas)
    plt.xlabel('Sim Time [sec]')
    plt.ylabel('Number of Measurements')
    plt.title('Measurement Number History')
    plt.savefig("../../Figures/meas_history_clutter.pdf", bbox_inches='tight', dpi=300)

    # fig, axs = plt.subplots(9, 1, figsize=(6,18))
    # for i,t_i in enumerate(t_vec):
    #     for j in range(len(axs)):
    #         if i==t_vec[-1]:
    #             axs[j].set_xlabel("Sim Time [sec]")
    #             axs[j].set_ylabel("Particle Error")
    #         for k in range(N_particles):
    #             axs[j].scatter(t_i,particle_error[k,i,j],particle_weight[k,i])

    plt.figure()
    plt.plot(t_vec,Neff)
    plt.xlabel('Sim Time [sec]')
    plt.ylabel('Number of Measurements')
    plt.title('Effective Sample Size')
    plt.grid()
    plt.savefig("../../Figures/eff_sample_size_clutter.pdf", bbox_inches='tight', dpi=300)

    plt.show()
