from RobotEnvironment import RobotEnvironment
from GMPHD_filter import GMPHD_filter_robotEnv
import numpy as np
from matplotlib import pyplot as plt
import time

if __name__ == '__main__':
    # Define initial state and time vector
    x_0 = np.array([[0.0],[0.0]])
    t_vec = np.arange(0, 1, 0.5)

    # Create the environment
    env = RobotEnvironment(n_features=20)

    # Compute ground truth and measurements
    X_k, Y_k = env.ground_truth(x_0, t_vec)

    feature_loc_test = env.inverse_measurement_model(Y_k[0][0], X_k[:,0])
    print(feature_loc_test)

    # Plot the ground truth
    fig1, fig2 = env.plot_ground_truth(X_k, Y_k, t_vec)

    fig3 = env.two_dim_visualizer(X_k)
    # plt.savefig("../../Figures/robot_env.png", bbox_inches='tight', dpi=300)

    twoD_Robot_filter = GMPHD_filter_robotEnv(env, birthCovariance=np.array([[5, 0],[0, 0.1]]), num_particles=30, max_features=20)
    mmse_error = []
    map_error = []
    sigma_x = []
    sigma_y = []
    num_meas = []
    Neff = []
    for i,y_k in enumerate(Y_k[1:]): # don't use measurements from initial position; start at tstep 1
        t0 = time.time()
        twoD_Robot_filter.filter(y_k)
        t1 = time.time()
        print("Tstep=%d in %.2f seconds" % (i+1, t1-t0))
        mmse_est, map_est, state_cov = twoD_Robot_filter.get_poseState()
        mmse_error.append(X_k[:,i+1]-mmse_est)
        map_error.append(X_k[:, i + 1] - map_est)
        Neff.append(twoD_Robot_filter.Neff)
        sigma_x.append(np.sqrt(state_cov[0,0]))
        sigma_y.append(np.sqrt(state_cov[1,1]))
        num_meas.append(len(y_k))

    mmse_err = np.array(mmse_error)
    map_err = np.array(map_error)
    Neff = np.array(Neff)
    sigma_x = np.array(sigma_x)
    sigma_y = np.array(sigma_y)

    fig, axs = plt.subplots(2,1)
    axs[0].plot(t_vec, mmse_err[:,0], 'r', label="MMSE Estimate")
    axs[0].plot(t_vec, map_err[:, 0], 'g', label="MAP Estimate")
    axs[0].plot(t_vec, 2*sigma_x, '--k')
    axs[0].plot(t_vec, -2*sigma_x, '--k', label=r'2$\sigma$')
    # axs[0].set_xlabel('Sim Time [sec]')
    axs[0].set_ylabel('Error [m]')
    axs[0].legend()
    axs[1].plot(t_vec, mmse_err[:, 1], 'r', label="MMSE Estimate")
    axs[1].plot(t_vec, map_err[:, 1], 'g', label="MAP Estimate")
    axs[1].plot(t_vec, 2*sigma_y, '--k')
    axs[1].plot(t_vec, -2*sigma_y, '--k', label=r'2$\sigma$')
    axs[1].set_xlabel('Sim Time [sec]')
    axs[1].set_ylabel('Error [m]')
    axs[1].legend()
    plt.savefig("../../Figures/robot_demo_state_error.pdf", bbox_inches='tight', dpi=300)
    fig.suptitle('Estimate Error')

    plt.figure()
    plt.plot(t_vec, num_meas)
    plt.xlabel('Sim Time [sec]')
    plt.ylabel('Number of Measurements')
    plt.title('Measurement Number History')
    plt.savefig("../../Figures/meas_history_robot.pdf", bbox_inches='tight', dpi=300)



    plt.figure()
    plt.plot(t_vec, Neff)
    plt.xlabel('Sim Time [sec]')
    plt.ylabel('Neff')
    plt.title('Effective Sample Size')
    plt.grid()

plt.show()