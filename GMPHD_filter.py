import numpy as np
from numpy.linalg import inv, pinv
from numpy.random import multivariate_normal as draw_mvn
from scipy.stats import multivariate_normal as mvn
from scipy.spatial.distance import mahalanobis
import copy
import RobotEnvironment
import SpacecraftEnvironment

class GMPHD_filter_robotEnv:
    """"
    The GMPHD_filter class represents the Gaussian Mixture Probability Hypothesis Density filter,
    as formulated in L. Schlenker's Thesis (https://conservancy.umn.edu/bitstream/handle/11299/206138/Schlenker_umn_0130M_20241.pdf?sequence=1)
    """
    def __init__(self, env:RobotEnvironment, birthCovariance, birthWeight=0.5, pS_inView=0.99, pS_outView=0.01, pD=0.99, Q=None, R=None, num_particles=100, max_features=50):
        self.num_particles = num_particles
        self.env = env
        self.particles = self._init_particles(num_particles)
        # self.poseState, self.poseCov = self.get_poseState()
        self.birthCovariance = birthCovariance
        self.birthWeight = birthWeight
        self.feature_cov = birthCovariance
        self.pD = pD # probability of detection
        self.pS_inView = pS_inView
        self.pS_outView = pS_outView
        if Q==None:
            self.Q = env.Q
        if R==None:
            self.R = np.array([[env.var_u, 0, 0], [0, env.var_v, 0], [0, 0, env.var_rho]])
        self.kappa = 1e-5 # clutter rate
        self.trunc_thresh = 1e-4
        self.merge_thresh = 1e-2
        self.max_features = max_features
        self.Na = round(0.4*num_particles) # threshold for number of effective particles

    def _init_particles(self, Ns):
        """
        Initialize the particles of the PF. Note that each particle is a pose state and a representation of the map (features list)
        """
        particles = []
        for _ in range(Ns):
            initial_state = np.zeros(self.env.state_size)
            particles.append({'Pose State': initial_state, 'Features': [], 'Particle Weight': 1/Ns}) # Features is a list of tuples (feature_state, feature_cov, feature_weight), in the inertial frame

        return particles

    def get_poseState(self):
        pose_mmse = np.zeros(self.particles[0]['Pose State'].shape)
        pose_map = self.particles[0]['Pose State']
        map = self.particles[0]['Particle Weight']
        total_mass = 0
        for i, particle_i in enumerate(self.particles):
            pose_mmse += particle_i['Pose State']*particle_i['Particle Weight']
            total_mass += particle_i['Particle Weight']
            if particle_i['Particle Weight']>map:
                pose_map = particle_i['Pose State']
                map = particle_i['Particle Weight']
        pose_mmse /= total_mass
        pose_cov = np.zeros([len(pose_mmse),len(pose_mmse)])
        for i, particle_i in enumerate(self.particles):
            diff_i = particle_i['Pose State']-pose_mmse
            pose_cov += np.outer(diff_i, diff_i)*particle_i['Particle Weight']/total_mass
        return pose_mmse, pose_map, pose_cov

    def filter(self, measurements):
        """
        Uses a measurement to update the feature states with GM PHD filter, and then the particle poses with a PF
        """
        for i in range(len(self.particles)):
            # Propagate particle state and covariance
            self.particles[i]['Pose State'] = self.env.integrate_dynamics(self.particles[i]['Pose State']) + np.random.multivariate_normal(np.array([0, 0]), self.env.Q)

            # New features, based on the features from the last timestep
            new_features = self._birth_model(self.particles[i]['Pose State'], measurements) # features which are independent from previous features
            new_features.extend(self._spawn_model()) # just to keep consistent with PHD filter
            new_features.extend(self._prediction_model(self.particles[i]['Features'], self.particles[i]['Pose State'])) # features predicted from previous features
            self.particles[i]['Features'] = []
            eta = []
            S = []
            K = []
            P_gk = []

            # Add new features to the particle's feature list, assuming they were NOT detected
            for ii,(m_gkm1,P_gkm1,w_gkm1) in enumerate(new_features): # add new features which were not detected given the state and features at k-1
                H = self.env.jacobianH(self.particles[i]['Pose State'], m_gkm1)
                H_xy = H[:, 0:2]
                X_c = np.concatenate([self.particles[i]['Pose State'], [0]])
                linearized_meas = np.array([self.env.Pu, self.env.Pv, 0]) - np.matmul(H,X_c) + np.matmul(H_xy,m_gkm1)
                nonlinear_meas = self.env.feature_to_measurement(m_gkm1, self.particles[i]['Pose State'])
                eta.append(nonlinear_meas)
                S.append(self.R + np.matmul(np.matmul(H_xy,P_gkm1),H_xy.T))
                K.append(np.matmul(np.matmul(P_gkm1,H_xy.T),pinv(S[ii])))
                P_gk.append(np.matmul((np.eye(H_xy.shape[1])-np.matmul(K[ii],H_xy)),P_gkm1))
                self.particles[i]['Features'].append((m_gkm1, P_gkm1, (1-self.pD)*w_gkm1))

            # Add new features to the particle's feature list, assuming they WERE detected
            for z in measurements: # add features which were detected (do a Kalman update on new features)
                Wzz_array = [self.pD*w_gkm1*mvn.pdf(z, mean=eta[j], cov=S[j], allow_singular=True) for j,(*_,w_gkm1) in enumerate(new_features)]
                for j,(m_gkm1,_,w_gkm1) in enumerate(new_features):
                    w = Wzz_array[j]/(self.kappa + sum(Wzz_array))
                    m = m_gkm1 + np.matmul(K[j],(z-eta[j]))
                    P = P_gk[j]
                    self.particles[i]['Features'].append((m, P, w))

            # Update particle weight (SC Filter)
            M_k = sum([w_gkm1 for (*_,w_gkm1) in new_features]) # likelihood of prediction of features
            gamma = 1
            for ii,z in enumerate(measurements):
                lambda_ = 0
                for j in range(len(new_features)):
                    xi = self.env.feature_to_measurement(new_features[j][0], self.particles[i]['Pose State'])
                    feature_cov = S[j]
                    lambda_ += mvn.pdf(z, mean=xi, cov=feature_cov, allow_singular=True)*new_features[j][2]
                gamma *= self.kappa + self.pD*lambda_
            self.particles[i]['Particle Weight'] *= np.exp(M_k)*gamma

            # Prune the features
            self.particles[i]['Features'] = self.prune_features(self.particles[i]['Features'], T=self.trunc_thresh, U=self.merge_thresh, Jmax = self.max_features)

        # Resample the particles
        self.resample()


    def _birth_model(self, pose_state_k, measurements_k): # TODO: not sure if I like this birth model...a more appropriate birth model for this problem would be to birth features near the edges of the camera frame, or wherever we would expect new features
        birth_features = []
        for j, y in enumerate(measurements_k):
            m_birth = self.env.inverse_measurement_model(y, pose_state_k)[0:2]
            P_birth = self.birthCovariance
            w_birth = self._get_birthWeight(m_birth)
            birth_features.append((m_birth, P_birth, w_birth))

        return birth_features

    def _get_birthWeight(self, x):
        """
        Returns the weight of a birthed feature based on the measurement
        param x: measurement in the state space (generated by the inverse measurement model)
        """
        w_birth = 0
        N = len(self.env.features)
        for i in range(self.env.features.shape[1]):
            w_birth += 1/N*mvn.pdf(x, mean=self.env.features[:,i], cov=self.feature_cov)

        return w_birth

    def _spawn_model(self):

        return []

    def _prediction_model(self, features_km1, pose_state):
        predicted_features = []
        for m_km1,P_km1,w_km1 in features_km1:
            m_k = m_km1
            P_k = P_km1
            # Set the probability of survival based on whether or not the feature is visible
            if self.env.is_visible(m_k, pose_state):
                pS = self.pS_inView
            else:
                pS = self.pS_outView
            w_k = w_km1*pS
            predicted_features.append((m_k, P_k, w_k))

        return predicted_features

    def prune_features(self, features, T, U, Jmax):
        features_I = []
        for feature_i in features:
            if feature_i[2]>T:
                features_I.append(feature_i)
        pruned_features = []
        while features_I:
            j = np.argmax([wk for *_,wk in features_I])
            features_L = []
            features_L_index = []
            wl_tilde = 0
            for i,feature_i in enumerate(features_I):
                mahalanobis_dist = mahalanobis(feature_i[0],features_I[j][0],inv(feature_i[1]))
                if mahalanobis_dist<=U:
                    features_L.append(feature_i)
                    features_L_index.append(i)
                    wl_tilde += feature_i[2]
            ml_tilde = 1/wl_tilde*sum([wi*mi for mi,_,wi in features_L])
            Pl_tilde = 1/wl_tilde*sum([wi*(Pi + (ml_tilde-mi)*(ml_tilde-mi).T) for mi,Pi,wi in features_L])
            for feature_index in reversed(features_L_index):
                del features_I[feature_index]
            pruned_features.append((ml_tilde, Pl_tilde, wl_tilde))
        if len(pruned_features)>Jmax:
            truncated_pruned_features = []
            for i in range(Jmax):
                j = np.argmax([wk for *_, wk in pruned_features])
                truncated_pruned_features.append(pruned_features[j])
                del pruned_features[j]
            pruned_features = truncated_pruned_features

        return pruned_features

    def resample(self):
        weight_sum = sum([particle_i['Particle Weight'] for particle_i in self.particles])
        for i in range(len(self.particles)):
            self.particles[i]['Particle Weight'] /= weight_sum
        weight_sum = sum([particle_i['Particle Weight'] for particle_i in self.particles])
        if (1-weight_sum)>1E-4:
            print('Error weight sum does not add to 1')
        self.Neff = 1/sum([particle_i['Particle Weight']**2 for particle_i in self.particles])
        if self.Neff<=self.Na:
            new_particles = []
            for _ in range(len(self.particles)):
                roll = np.random.uniform()
                ii = 0
                beta = self.particles[0]['Particle Weight']
                while roll>=beta:
                    ii += 1
                    beta += self.particles[ii]['Particle Weight']
                particle_ii = copy.deepcopy(self.particles[ii])
                particle_ii['Pose State'] = particle_ii['Pose State']
                particle_ii['Particle Weight'] = 1 / self.num_particles
                new_particles.append(particle_ii)
            self.particles = new_particles

        return

class GMPHD_filter_scEnv:
    """"
    The GMPHD_filter class represents the Gaussian Mixture Probability Hypothesis Density filter,
    as formulated in L. Schlenker's Thesis (https://conservancy.umn.edu/bitstream/handle/11299/206138/Schlenker_umn_0130M_20241.pdf?sequence=1)
    """
    def __init__(self, env:SpacecraftEnvironment, initial_state, birthCovariance, birthWeight=0.5, pS_inView=0.99, pS_outView=0.01, pD=0.99, Q=None, R=None, num_particles=100, max_features=50):
        self.num_particles = num_particles
        self.env = env
        self.particles = self._init_particles(initial_state, num_particles)
        self.poseState, self.poseMap, self.poseCov = self.get_poseState()
        self.birthCovariance = birthCovariance
        self.birthWeight = birthWeight
        self.feature_cov = birthCovariance
        self.pD = pD # probability of detection
        self.pS_inView = pS_inView
        self.pS_outView = pS_outView
        if Q==None:
            self.Q = env.Q
        if R==None:
            self.R = np.array([[env.var_u, 0, 0], [0, env.var_v, 0], [0, 0, env.var_rho]])
        # self.kappa = 4e-2 # clutter rate
        self.kappa = 1e-3 # clutter rate

        self.trunc_thresh = 1e-4
        self.merge_thresh = 1e-2
        self.max_features = max_features
        self.Na = round(0.6*num_particles) # threshold for number of effective particles

    def _init_particles(self, initial_state, Ns):
        """
        Initialize the particles of the PF. Note that each particle is a pose state and a representation of the map (features list)
        """
        particles = []
        for _ in range(Ns):
            initial_state = initial_state + np.random.multivariate_normal(np.zeros(initial_state.shape), self.env.Q)
            particles.append({'Pose State': initial_state, 'Features': [], 'Particle Weight': 1/Ns}) # Features is a list of tuples (feature_state, feature_cov, feature_weight), in the inertial frame

        return particles

    def get_poseState(self):
        pose_mmse = np.zeros(self.particles[0]['Pose State'].shape)
        total_mass = 0
        pose_map = self.particles[0]['Pose State']
        map = self.particles[0]['Particle Weight']
        for i, particle_i in enumerate(self.particles):
            pose_mmse += particle_i['Pose State']*particle_i['Particle Weight']
            total_mass += particle_i['Particle Weight']
            if particle_i['Particle Weight']>map:
                pose_map = particle_i['Pose State']
                map = particle_i['Particle Weight']
        pose_mmse /= total_mass
        pose_cov = np.zeros([len(pose_mmse),len(pose_mmse)])
        for i, particle_i in enumerate(self.particles):
            diff_i = particle_i['Pose State']-pose_mmse
            pose_cov += np.outer(diff_i, diff_i)*particle_i['Particle Weight']/total_mass
        return pose_mmse, pose_map, pose_cov

    def filter(self, measurements):
        """
        Uses a measurement to update the feature states with GM PHD filter, and then the particle poses with a PF
        """
        for i in range(len(self.particles)):
            # Propagate particle state and covariance
            self.particles[i]['Pose State'] = self.env.integrate_dynamics(self.particles[i]['Pose State']) + np.random.multivariate_normal(np.zeros(self.particles[i]['Pose State'].shape), self.env.Q)

            # New features, based on the features from the last timestep
            new_features = self._birth_model(self.particles[i]['Pose State'], measurements) # features which are independent from previous features
            new_features.extend(self._spawn_model()) # just to keep consistent with PHD filter
            new_features.extend(self._prediction_model(self.particles[i]['Features'], self.particles[i]['Pose State'])) # features predicted from previous features
            self.particles[i]['Features'] = []
            eta = []
            S = []
            K = []
            P_gk = []

            # Add new features to the particle's feature list, assuming they were NOT detected
            for ii,(m_gkm1,P_gkm1,w_gkm1) in enumerate(new_features): # add new features which were not detected given the state and features at k-1
                H = self.env.jacobianH(self.particles[i]['Pose State'], m_gkm1)
                X_c = self.particles[i]['Pose State'][:3]
                nonlinear_meas = self.env.feature_to_measurement(m_gkm1, self.particles[i]['Pose State'])
                eta.append(nonlinear_meas)
                S.append(self.R + np.matmul(np.matmul(H,P_gkm1),H.T))
                K.append(np.matmul(np.matmul(P_gkm1,H.T),pinv(S[ii])))
                P_gk.append(np.matmul((np.eye(H.shape[1])-np.matmul(K[ii],H)),P_gkm1))
                self.particles[i]['Features'].append((m_gkm1, P_gkm1, (1-self.pD)*w_gkm1))

            # Add new features to the particle's feature list, assuming they WERE detected
            for z in measurements: # add features which were detected (do a Kalman update on new features)
                Wzz_array = [self.pD*w_gkm1*mvn.pdf(z, mean=eta[j], cov=S[j], allow_singular=True) for j,(*_,w_gkm1) in enumerate(new_features)]
                for j,(m_gkm1,_,w_gkm1) in enumerate(new_features):
                    w = Wzz_array[j]/(self.kappa + sum(Wzz_array))
                    m = m_gkm1 + np.matmul(K[j],(z-eta[j]))
                    P = P_gk[j]
                    self.particles[i]['Features'].append((m, P, w))

            # Update particle weight (SC Filter)
            M_k = sum([w_gkm1 for (*_,w_gkm1) in new_features]) # likelihood of prediction of features
            gamma = 1
            for ii,z in enumerate(measurements):
                lambda_ = 0
                for j in range(len(new_features)):
                    xi = self.env.feature_to_measurement(new_features[j][0], self.particles[i]['Pose State'])
                    feature_cov = S[j]
                    lambda_ += mvn.pdf(z, mean=xi, cov=feature_cov, allow_singular=True)*new_features[j][2]
                gamma *= self.kappa + self.pD*lambda_
            self.particles[i]['Particle Weight'] *= np.exp(M_k)*gamma
            # print("\nCompleted Particle "+str(int(i+1)))

            # Prune the features
            self.particles[i]['Features'] = self.prune_features(self.particles[i]['Features'], T=self.trunc_thresh, U=self.merge_thresh, Jmax = self.max_features)

        # Resample the particles
        self.resample()


    def _birth_model(self, pose_state_k, measurements_k): # TODO: not sure if I like this birth model...a more appropriate birth model for this problem would be to birth features near the edges of the camera frame, or wherever we would expect new features
        birth_features = []
        for j, y in enumerate(measurements_k):
            m_birth = self.env.inverse_measurement_model(y, pose_state_k)
            P_birth = self.birthCovariance
            w_birth = self._get_birthWeight(m_birth)
            birth_features.append((m_birth, P_birth, w_birth))

        return birth_features

    def _get_birthWeight(self, x):
        """
        Returns the weight of a birthed feature based on the measurement
        param x: measurement in the state space (generated by the inverse measurement model)
        """
        w_birth = 0
        N = len(self.env.features)
        for i in range(self.env.features.shape[1]):
            w_birth += 1/N*mvn.pdf(x, mean=self.env.features[:,i], cov=self.feature_cov)

        return w_birth

    def _spawn_model(self):

        return []

    def _prediction_model(self, features_km1, pose_state):
        predicted_features = []
        for m_km1,P_km1,w_km1 in features_km1:
            m_k = m_km1
            P_k = P_km1
            # Set the probability of survival based on whether or not the feature is visible
            if self.env.is_visible(m_k, pose_state):
                pS = self.pS_inView
            else:
                pS = self.pS_outView
            w_k = w_km1*pS
            predicted_features.append((m_k, P_k, w_k))

        return predicted_features

    def prune_features(self, features, T, U, Jmax):
        features_I = []
        for feature_i in features:
            if feature_i[2]>T:
                features_I.append(feature_i)
        pruned_features = []
        while features_I:
            j = np.argmax([wk for *_,wk in features_I])
            features_L = []
            features_L_index = []
            wl_tilde = 0
            for i,feature_i in enumerate(features_I):
                mahalanobis_dist = mahalanobis(feature_i[0],features_I[j][0],inv(feature_i[1]))
                if mahalanobis_dist<=U:
                    features_L.append(feature_i)
                    features_L_index.append(i)
                    wl_tilde += feature_i[2]
            ml_tilde = 1/wl_tilde*sum([wi*mi for mi,_,wi in features_L])
            Pl_tilde = 1/wl_tilde*sum([wi*(Pi + (ml_tilde-mi)*(ml_tilde-mi).T) for mi,Pi,wi in features_L])
            for feature_index in reversed(features_L_index):
                del features_I[feature_index]
            pruned_features.append((ml_tilde, Pl_tilde, wl_tilde))
        if len(pruned_features)>Jmax:
            truncated_pruned_features = []
            for i in range(Jmax):
                j = np.argmax([wk for *_, wk in pruned_features])
                truncated_pruned_features.append(pruned_features[j])
                del pruned_features[j]
            pruned_features = truncated_pruned_features

        return pruned_features

    def resample(self):
        weight_sum = sum([particle_i['Particle Weight'] for particle_i in self.particles])
        for i in range(len(self.particles)):
            self.particles[i]['Particle Weight'] /= weight_sum
        weight_sum = sum([particle_i['Particle Weight'] for particle_i in self.particles])
        if (1-weight_sum)>1E-4:
            print('Error weight sum does not add to 1')
        self.Neff = 1/sum([particle_i['Particle Weight']**2 for particle_i in self.particles])
        if self.Neff<=self.Na:
            new_particles = []
            for _ in range(len(self.particles)):
                roll = np.random.uniform()
                ii = 0
                beta = self.particles[0]['Particle Weight']
                while roll>=beta:
                    ii += 1
                    beta += self.particles[ii]['Particle Weight']
                particle_ii = copy.deepcopy(self.particles[ii])
                particle_ii['Pose State'] = particle_ii['Pose State']
                particle_ii['Particle Weight'] = 1 / self.num_particles
                new_particles.append(particle_ii)
            self.particles = new_particles
            print("Resampled particles!")
        return