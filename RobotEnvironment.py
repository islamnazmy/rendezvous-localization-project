import numpy as np
from matplotlib import pyplot as plt
import random
from shapely.geometry import Polygon, Point
import math as m

class RobotEnvironment:
    """
    The RobotEnvironment class provides the measurement model, ground truth, dynamics, and features for the filtering problem
    """
    def __init__(self, n_features=40):
        # Define features
        self.n_features = n_features
        self.x_bounds = [0.0, 150.0]
        self.y_bounds = [-15.0, 15.0]
        self.state_size = 2
        self.fov = 180 # deg
        self.features = np.zeros((2, self.n_features))
        for idx in range(0, self.n_features):
            y_idx = random.randint(0,1)
            self.features[0, idx] = random.uniform(self.x_bounds[0], self.x_bounds[1])
            self.features[1, idx] = self.y_bounds[y_idx]

        # Define camera model parameters
        self.CN = np.array([[0.0, -1.0, 0.0], [0.0, 0.0, -1.0], [1.0, 0.0, 0.0]]) # Inertial frame to camera frame DCM
        self.f = 25.0/1000.0 # m
        self.Pu = 128.0
        self.Pv = 128.0
        self.s = 0.0
        self.mu = 20480.0 # m^-1
        self.mv = 20480.0 # m^-1
        self.var_u = 5.0
        self.var_v = 5.0
        self.var_rho = 5.0
        self.A = np.array([[self.f*self.mu, self.s, self.Pu],
                           [0.0, self.f*self.mv, self.Pv],
                           [0.0, 0.0, 1.0]])

        # Define process noise
        self.Q = np.array([[0.5, 0.0] ,[0.0, 0.25]])

    def ground_truth(self, x_0, t_vec):
        """Generates a ground truth of states and measurements for the entire timestep
        Inputs: """

        # Initialize a sequence of states
        X_k = np.zeros((2, len(t_vec)+1))
        X_k[:,0] = x_0[:,0]

        # Compute each ground truth
        for idx, t in enumerate(t_vec):
            X_k[:,idx+1] = self.integrate_dynamics(X_k[:,idx]) + np.random.multivariate_normal(np.array([0, 0]), self.Q)

        # Generate a sequence of measurements
        Y_k = self.measurement_model(X_k)

        return X_k, Y_k

    def measurement_model(self, X_k, clutter=False):
        """Creates a sequence of measurements, one for each state
        Inputs:
            sequence of states

        Output:
            sequence of measurements (list of np-arrays)
            First three entries is full x-vector from reference paper - [wu; wv; w], noise added
            Fourth entry is rho"""

        Y_k = []
        for idx,_ in enumerate(X_k[0,:]):
            visible_features = self.visible_features(X_k[:,idx])
            Y_temp = []
            for feature in visible_features:
                X = np.concatenate((feature, np.array([0])))
                X_c = np.concatenate((X_k[:,idx], np.array([0])))
                x = np.matmul(np.matmul(self.A, self.CN), X-X_c)
                u = x[0]/x[2] + np.random.normal(0, np.sqrt(self.var_u))
                while u<0 or u>2*self.Pu: # hack to keep measurements continuous but within the bounds after noise
                    u = x[0] / x[2] + np.random.normal(0, np.sqrt(self.var_u))
                v = x[1]/x[2] + np.random.normal(0, np.sqrt(self.var_v))
                while v<0 or v>2*self.Pv:
                    v = x[1] / x[2] + np.random.normal(0, np.sqrt(self.var_v))
                rho = np.linalg.norm(X-X_c)+np.random.normal(0, np.sqrt(self.var_rho))
                while rho<0:
                    rho = np.linalg.norm(X - X_c) + np.random.normal(0, np.sqrt(self.var_rho))
                Y_temp.append(np.hstack([u, v, rho]))
                # H = self.jacobianH(X_c[0:2], feature)
                # H_xy = H[:,0:2]
                # linearized_meas = np.array([self.Pu, self.Pv, 0]) - np.matmul(H,X_c) + np.matmul(H_xy,feature)
                # print([u,v,rho])
                # print(linearized_meas)
            Y_k.append(Y_temp)

        return Y_k

    def feature_to_measurement(self, feature, X_k):
        """
        Converts feature in inertial space to measurement in measurement space
        Does not include noise
        """
        X = np.concatenate((feature, np.array([0])))
        X_c = np.concatenate((X_k, np.array([0])))
        x = np.matmul(np.matmul(self.A, self.CN), X - X_c)
        measurement = np.hstack([x[0]/x[2], x[1]/x[2], np.linalg.norm(X - X_c)])

        return measurement

    def jacobianH(self, X_c, X):
        """
        Jacobian of the measurement model
        Inputs:
            X_c     Camera position (2-D)
            X     Feature position (2-D)
        Output:
            H_k     3x2 Jacobian of linearized measurement model
        """
        X_rel_I = np.concatenate([X-X_c,[0]])
        rho = np.linalg.norm(X_rel_I)
        M = np.array([[self.f*self.mu/rho, 0, 0], [0, self.f*self.mv/rho, 0], [0, 0, 1]])
        H = np.array([np.matmul(M,self.CN)[0,:], np.matmul(M,self.CN)[1,:], [X_rel_I[0]/rho, X_rel_I[1]/rho, 0]])

        return H

    def inverse_measurement_model(self, Y_k, X_k):
        """"
        Returns the position of the feature given the measurement and robot position
        Y_k: measurement (u,v,rho)
        X_k: Pose state in the inertial frame

        Outputs the measurement feature position in the inertial frame
        """
        u = Y_k[0]
        v = Y_k[1]
        rho = Y_k[2]
        a = (u-self.Pu)/(self.f*self.mu)
        b = (v-self.Pv)/(self.f*self.mv)
        Zi = np.sqrt(rho**2/(a**2+b**2+1))
        Xi = a*Zi
        Yi = b*Zi
        feature_position = np.concatenate((X_k, np.array([0]))) + np.matmul(np.linalg.inv(self.CN),np.array([Xi,Yi,Zi]))
        return feature_position


    def visible_features(self, x):
        """Computes which features are visible to the robot at a given state"""
        vis_features = []

        for idx in range(self.n_features):
            if self.is_visible(self.features[:,idx], x):
                vis_features.append(self.features[:,idx])

        return vis_features

    def is_visible(self, feature, state):
        relFeature_I = np.hstack((feature-state, np.array([0]))) # relative position of the feature wrt camera in the inertial frame
        feature_CN = np.matmul(self.CN, relFeature_I) # feature in the camera frame
        if feature_CN[2]>0:
            x = np.matmul(self.A, feature_CN)
            u = x[0]/x[2]
            v = x[1]/x[2]
            if u>0 and u<2*self.Pu and v>0 and v<2*self.Pv:
                return True
            else:
                return False
        else:
            return False

    def integrate_dynamics(self, x_k):
        """Integrates robot dynamics forward by one time step"""
        x_k1 = np.array([x_k[0]+1,
                         x_k[1]])

        return x_k1

    def plot_ground_truth(self, X_k, Y_k, t_vec):
        """Plots the state and measurements vs time - two state plots and a single plot of the measurements"""
        fig1, ax1 = plt.subplots(figsize=(6, 4))
        plt.plot(t_vec, X_k[0,1:], label='X')
        plt.xlabel('Time (s)')
        plt.ylabel('X (m)')

        fig2, ax2 = plt.subplots(figsize=(6, 4))
        plt.plot(t_vec, X_k[1,1:], label='Y')
        plt.xlabel('Time (s)')
        plt.ylabel('Y (m)')

        return fig1, fig2

    def two_dim_visualizer(self, X_k):
        """Visualizes the robot and it's motion in two dimensions"""

        fig, ax = plt.subplots(figsize=(6, 4))

        # Plot the features
        plt.scatter(self.features[0,:], self.features[1,:], marker='x', color='r')

        # Plot the state evolution
        plt.plot(X_k[0,:], X_k[1,:])
        ax.set_xlabel('X (m)')
        ax.set_ylabel('Y (m)')

        return fig
