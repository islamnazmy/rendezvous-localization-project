import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt
import random
from shapely.geometry import Polygon, Point
import math as m
import trimesh

class SpacecraftEnvironment:
    """
    The SpacecraftEnvironment class provides the measurement model, ground truth, dynamics, and features for the filtering problem
    """
    def __init__(self):
        # Define target body parameters
        self.target_radius = 50 # m

        # Define features
        self.n_features = 12
        self.features = np.zeros((3, self.n_features))
        self.feature_boundaries = [[-100, 100], [-100, 100], [-100, 100]] # m
        self.initialize_features()

        # Define camera model parameters
        self.f = 25.0/1000.0 # m
        self.Pu = 128.0
        self.Pv = 128.0
        self.s = 0.0
        self.mu = 20480.0 # m^-1
        self.mv = 20480.0 # m^-1
        self.var_u = 1.0
        self.var_v = 1.0
        self.var_rho = 1.0
        self.A = np.array([[self.f*self.mu, self.s, self.Pu],
                           [0.0, self.f*self.mv, self.Pv],
                           [0.0, 0.0, 1.0]])

        # Define process noise
        self.Q = np.array([[0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.001, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])

        # Define dynamics parameters
        self.mu_ = 398600.4418 # km^3/m^2
        self.a = 7080.0 # km
        self.n = np.sqrt(self.mu_/(self.a**3))
        self.dt = 10.0 # s

        # Turns verbosity on or off to check if features are not visible to the spacecraft
        self.verbose = False

        self.non_spherical = False
        self.target_mesh = None
        self.n_clutter = 3

    def initialize_features(self):
        """"Initializes a np array of features in x-y-z space"""
        for idx in range(0, self.n_features):
            # Create a temporary feature to perform mathematical operations
            temp_feature = np.zeros((3, 1))

            # Pull the feature location from uniform distributions
            temp_feature[0, 0] = random.uniform(self.feature_boundaries[0][0], self.feature_boundaries[0][1])
            temp_feature[1, 0] = random.uniform(self.feature_boundaries[1][0], self.feature_boundaries[1][1])
            temp_feature[2, 0] = random.uniform(self.feature_boundaries[2][0], self.feature_boundaries[2][1])

            # Normalize the temporary feature and multiply it by the target radius
            temp_feature = self.target_radius*temp_feature/np.linalg.norm(temp_feature)

            # Assign the feature to self.features
            self.features[:, idx] = temp_feature[:,0]

    def ground_truth(self, x_0, t_vec, clutter=False):
        """Generates a ground truth of states and measurements for the entire timestep
        Inputs: """

        # Initialize a sequence of states
        X_k = np.zeros((9, len(t_vec)+1))
        X_k[:,0] = x_0[:,0]

        # Compute each ground truth
        for idx, t in enumerate(t_vec):
            X_k[:,idx+1] = self.integrate_dynamics(X_k[:,idx]) + np.random.multivariate_normal(np.zeros((9)), self.Q)
            # X_k[:,idx+1] = self.integrate_dynamics(X_k[:,idx])

        # Generate a sequence of measurements
        Y_k = self.measurement_model(X_k, clutter)

        return X_k, Y_k

    def integrate_dynamics(self, x_k):
        """Integrates spacecraft dynamics forward by one time step using RK4 integration"""
        new_state= np.zeros((9, 1))

        k1 = self.dt*self.equations_of_motion(x_k[0:6])
        k2 = self.dt*self.equations_of_motion(x_k[0:6] + 0.5*k1[:,0])
        k3 = self.dt*self.equations_of_motion(x_k[0:6] + 0.5*k2[:,0])
        k4 = self.dt*self.equations_of_motion(x_k[0:6] + k3[:,0])

        new_state[0:6, 0] = x_k[0:6] + (1.0/6.0)*(k1[:,0] + 2*k2[:,0] + 2*k3[:,0] + k4[:,0])
        # new_state = np.vstack((new_state, self.dcm_to_mrp(self.compute_dcm(new_state))))
        new_state[6:, 0] = self.dcm_to_mrp(self.compute_dcm(new_state))[0:3,0]
        # new_state = x_k + k1[:,0]

        # MRP switching
        # if np.linalg.norm(new_state[6:]) > 1.0:
        #     new_state[6:] = -new_state[6:]/(np.linalg.norm(new_state[6:])**2)

        return new_state[:,0]

    def equations_of_motion(self, x_k):
        """"Returns the x_dot vector"""
        # Initialize x
        x_dot = np.zeros((6, 1))

        # Velocity
        x_dot[0, 0] = x_k[3]
        x_dot[1, 0] = x_k[4]
        x_dot[2, 0] = x_k[5]

        # Acceleration
        x_dot[3, 0] = 3*x_k[0]*self.n**2 + 2*x_k[4]*self.n
        x_dot[4, 0] = -2*x_k[3]*self.n
        x_dot[5, 0] = -x_k[2]*self.n**2

        # MRPs
        # omega = np.matmul(self.mrp_to_dcm(x_k[6:]), np.cross(x_k[0:3], x_k[3:6])/(np.linalg.norm(x_k[0:3])**2))
        # # omega = np.cross(x_k[0:3], x_k[3:6])/(np.linalg.norm(x_k[0:3])**2)
        # x_dot[6:,0] = 0.25*np.matmul(((1-np.linalg.norm(x_k[6:])**2)*np.diag(np.ones(3)) + 2.0*self.skew_sym(x_k[6:]) + np.matmul(x_k[6:], np.transpose(x_k[6:]))), omega)

        return x_dot

    def skew_sym(self, vec):
        """Returns the skew symmetric matrix representation of a vector"""
        return np.array([[0, -vec[2], vec[1]],
                         [vec[2], 0, -vec[0]],
                         [-vec[1], vec[0], 0]])

    def measurement_model(self, X_k, clutter=False):
        """Creates a sequence of measurements, one for each state
        Inputs:
            sequence of states

        Output:
            sequence of measurements (list of np-arrays)
            First three entries is full x-vector from reference paper - [wu; wv; w], noise added
            Fourth entry is rho"""

        Y_k = []
        for idx, _ in enumerate(X_k[0,:]):
            visible_features = self.visible_features(X_k[:,idx])
            Y_temp = []
            for feature in visible_features:
                y = self.feature_to_measurement(feature, X_k[:,idx])

                # Compute u, keep computing until its within the bounds of the sensor plane
                u = y[0] + np.random.normal(0, np.sqrt(self.var_u))
                while u<0 or u>2*self.Pu: # hack to keep measurements continuous but within the bounds
                    u = y[0] + np.random.normal(0, np.sqrt(self.var_u))

                # Compute v, keep computing until its within the bounds of the sensor plane
                v = y[1] + np.random.normal(0, np.sqrt(self.var_v))
                while v<0 or v>2*self.Pv:
                    v = y[1] + np.random.normal(0, np.sqrt(self.var_v))

                # Compute rho, keep computing until its greater than 0
                rho = y[2] + np.random.normal(0, np.sqrt(self.var_rho))
                while rho<0:
                    rho = y[2] + np.random.normal(0, np.sqrt(self.var_rho))

                # Append to y-temp
                Y_temp.append(np.hstack([u, v, rho]))

            if clutter:
                for i in range(self.n_clutter):
                    # Create a temporary feature to perform mathematical operations
                    temp_feature = np.zeros((3, 1))

                    # Pull the feature location from uniform distributions
                    temp_feature[0, 0] = random.uniform(self.feature_boundaries[0][0], self.feature_boundaries[0][1])
                    temp_feature[1, 0] = random.uniform(self.feature_boundaries[1][0], self.feature_boundaries[1][1])
                    temp_feature[2, 0] = random.uniform(self.feature_boundaries[2][0], self.feature_boundaries[2][1])

                    if self.is_visible(temp_feature[:,0], X_k[:,idx]):
                        y = self.feature_to_measurement(temp_feature[:,0], X_k[:,idx])

                        # Compute u, keep computing until its within the bounds of the sensor plane
                        u = y[0] + np.random.normal(0, np.sqrt(self.var_u))
                        while u<0 or u>2*self.Pu: # hack to keep measurements continuous but within the bounds
                            u = y[0] + np.random.normal(0, np.sqrt(self.var_u))

                        # Compute v, keep computing until its within the bounds of the sensor plane
                        v = y[1] + np.random.normal(0, np.sqrt(self.var_v))
                        while v<0 or v>2*self.Pv:
                            v = y[1] + np.random.normal(0, np.sqrt(self.var_v))

                        # Compute rho, keep computing until its greater than 0
                        rho = y[2] + np.random.normal(0, np.sqrt(self.var_rho))
                        while rho<0:
                            rho = y[2] + np.random.normal(0, np.sqrt(self.var_rho))

                        # Append to y-temp
                        Y_temp.append(np.hstack([u, v, rho]))

            Y_k.append(Y_temp)

        return Y_k

    def visible_features(self, x):
        """Computes which features are visible to the robot at a given state"""
        vis_features = []

        for idx in range(self.n_features):
            if self.is_visible(self.features[:,idx], x):
                vis_features.append(self.features[:,idx])

        return vis_features

    def is_visible(self, feature, state):
        """Spacecraft visibility model
        A feature is visible if it is on the spacecraft side of the body
        It is also only visible if it falls on the detector array"""
        # Compute measurement features
        measurement = self.feature_to_measurement(feature, state)
        u = measurement[0]
        v = measurement[1]


        if self.non_spherical:
            # If a ray from the state to the feature intersects the mesh, it's not visible
            intersector = trimesh.ray.ray_triangle.RayMeshIntersector(self.target_mesh)
            if intersector.intersects_any(feature.reshape(1,3), (state[0:3]-feature).reshape(1,3)):
                return False
        else:
            # If the distance from the feature to the spacecraft is greater than the distance to the center of the circle,
            # it's not visible
            if np.linalg.norm(state[0:3]-feature) > np.linalg.norm(state[0:3]):
                if self.verbose:
                    print('Not visible')
                return False

        if u < 0 or u > 2*self.Pu:
            if self.verbose:
                print('Not in plane', u, self.Pu)
            return False
        elif v < 0 or v > 2*self.Pv:
            if self.verbose:
                print('Not in plane', v, self.Pv)
            return False
        else:
            return True

    def feature_to_measurement(self, feature, X_k):
        """
        Converts feature in inertial space to measurement in measurement space
        Does not include noise
        """
        X = feature
        X_c = X_k[0:3]
        CH = self.mrp_to_dcm(X_k[6:])
        x = np.matmul(np.matmul(self.A, CH), X - X_c)

        return np.hstack([x[0]/x[2], x[1]/x[2], np.linalg.norm(X - X_c)])

    def jacobianH(self, X_c, X):
        """
        Jacobian of the measurement model
        Inputs:
            X_c     Camera position (3-D)
            X     Feature position (3-D)
        Output:
            H_k     3x3 Jacobian of linearized measurement model
        """
        X_rel_I = X-X_c[:3]
        rho = np.linalg.norm(X_rel_I)
        M = np.array([[self.f*self.mu/rho, 0, 0], [0, self.f*self.mv/rho, 0], [0, 0, 1]])
        CH = self.mrp_to_dcm(X_c[6:])
        H = np.array([np.matmul(M,CH)[0,:], np.matmul(M,CH)[1,:], [X_rel_I[0]/rho, X_rel_I[1]/rho, X_rel_I[2]/rho]])

        return H

    def inverse_measurement_model(self, Y_k, X_k):
        """"
        Returns the position of the feature given the measurement and robot position
        Y_k: measurement (u,v,rho)
        X_k: Pose state in the inertial frame

        Outputs the measurement feature position in the inertial frame
        """
        u = Y_k[0]
        v = Y_k[1]
        rho = Y_k[2]
        a = (u-self.Pu)/(self.f*self.mu)
        b = (v-self.Pv)/(self.f*self.mv)
        Zi = np.sqrt(rho**2/(a**2+b**2+1))
        Xi = a*Zi
        Yi = b*Zi
        CH = self.mrp_to_dcm(X_k[6:])
        feature_position = X_k[:3] + np.matmul(np.linalg.inv(CH), np.array([Xi, Yi, Zi]))
        return feature_position

    def visualize(self, X_k, close_up=False):
        """Visualizes the spacecraft trajectory, target, and features"""

        fig = plt.figure(figsize=(6, 4))
        ax = plt.axes(projection='3d')

        if self.non_spherical:
            ax.plot_trisurf(self.target_mesh.vertices[:, 0], self.target_mesh.vertices[:,1], triangles=self.target_mesh.faces, Z=self.target_mesh.vertices[:,2])
        else:
            # Plot a sphere
            u = np.linspace(0, 2 * np.pi, 100)
            v = np.linspace(0, np.pi, 100)
            x = (self.target_radius-1.0)*np.outer(np.cos(u), np.sin(v))
            y = (self.target_radius-1.0)*np.outer(np.sin(u), np.sin(v))
            z = (self.target_radius-1.0)*np.outer(np.ones(np.size(u)), np.cos(v))
            ax.plot_surface(x, y, z, linewidth=0.0)

        # Plot the features
        ax.scatter(self.features[0,:], self.features[1,:], self.features[2,:], marker='x', color='r', s=10)

        # Plot the state evolution
        ax.plot(X_k[0,:], X_k[1,:], X_k[2,:])

        ax.set_xlabel('X (m)')
        ax.set_ylabel('Y (m)')
        ax.set_zlabel('Z (m)')

        ax.set_xlim(-5, 5)
        ax.set_ylim(-5, 5)
        ax.set_zlim(-5, 5)

        self.set_axes_equal(ax)

        return fig, ax

    def mrp_to_dcm(self, sigma):
        """Returns the DCM corresponding to the MRP rotation input into the function"""
        return np.diag(np.ones(3)) + (8.0*np.matmul(self.skew_sym(sigma), self.skew_sym(sigma)) - 4.0*(1-np.linalg.norm(sigma)**2)*self.skew_sym(sigma))/((1+np.linalg.norm(sigma)**2)**2)

    def dcm_to_mrp(self, C):
        """Returns the MRP attitude representation corresponding to the DCM input"""
        zeta = np.sqrt(np.trace(C)+1)
        return np.array([[C[1,2]-C[2,1]], [C[2,0]-C[0,2]], [C[0,1]-C[1,0]]])/(zeta*(zeta+2))

    def compute_dcm(self, X):
        """Computes the DCM, foregoing the MRP DKE and fixing the attitude instead
        Aligns the b_hat_3 axis to the negative spacecraft position vector
        The b_hat_2 axis is the cross product of b_hat_3 and the velocity vector
        The b_hat_1 vector is the cross product of b_hat_2 and b_hat_3"""
        b_hat_3 = -X[0:3].T/np.linalg.norm(X[0:3])

        b_hat_2  = -np.cross(b_hat_3, X[3:6].T/np.linalg.norm(X[3:6]))
        b_hat_2 = b_hat_2/np.linalg.norm(b_hat_2)

        b_hat_1 = np.cross(b_hat_2, b_hat_3)
        b_hat_1 = b_hat_1/np.linalg.norm(b_hat_1)

        C = np.zeros((3,3))

        C[0,:] = b_hat_1
        C[1,:] = b_hat_2
        C[2,:] = b_hat_3

        return C

    def load_object(self):
        # We are now using a non_spherical body
        self.non_spherical = True

        # Load the mesh, dump the geometry, and concatenate it into one mesh
        mesh = trimesh.load_mesh('Satellite.obj')
        dump = mesh.dump()
        self.target_mesh = dump.sum()

        # Apply transformation to point antenna array in nadir direction
        homog_transform = np.array([[0.0, 1.0, 0.0, 0.0], [0.0, 0.0, -1.0, 0.0], [-1.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
        self.target_mesh.apply_transform(homog_transform)

        # Reinitialize the features
        self.features = np.zeros((3, self.n_features))

        # Solar panels
        self.features[:, 0] = np.array([-0.4, 4.0, 0.0])
        self.features[:, 1] = np.array([0.1, 4.0, 0.0])
        self.features[:, 2] = np.array([-0.4, -4.0, 0.0])
        self.features[:, 3] = np.array([0.1, -4.0, 0.0])

        # Body
        self.features[:, 4] = np.array([1.3, 0.0, 1.5])
        self.features[:, 5] = np.array([-1.3, 0.0, 1.5])

        # Antenna array
        self.features[:, 6] = np.array([2.0, 0.0, -2.9])
        self.features[:, 7] = np.array([-2.0, 0.0, -2.9])


    def set_axes_equal(self, ax):
        '''
        Source: https://stackoverflow.com/questions/13685386/matplotlib-equal-unit-length-with-equal-aspect-ratio-z-axis-is-not-equal-to
        Make axes of 3D plot have equal scale so that spheres appear as spheres,
        cubes as cubes, etc..  This is one possible solution to Matplotlib's
        ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

        Input
          ax: a matplotlib axis, e.g., as output from plt.gca().
        '''

        x_limits = ax.get_xlim3d()
        y_limits = ax.get_ylim3d()
        z_limits = ax.get_zlim3d()

        x_range = abs(x_limits[1] - x_limits[0])
        x_middle = np.mean(x_limits)
        y_range = abs(y_limits[1] - y_limits[0])
        y_middle = np.mean(y_limits)
        z_range = abs(z_limits[1] - z_limits[0])
        z_middle = np.mean(z_limits)

        # The plot bounding box is a sphere in the sense of the infinity
        # norm, hence I call half the max range the plot radius.
        plot_radius = 0.5*max([x_range, y_range, z_range])

        ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
        ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
        ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

    def plot_ground_truth(self, X_k, Y_k, t_vec):
        """Plots the state and measurements vs time - two state plots and a single plot of the measurements"""
        fig1, ax1 = plt.subplots(figsize=(6, 4))
        plt.plot(t_vec, X_k[6,1:], label='X')
        plt.xlabel('Time (s)')
        fig1.gca().set_ylabel(r'$\sigma_1$')

        fig2, ax2 = plt.subplots(figsize=(6, 4))
        plt.plot(t_vec, X_k[7,1:], label='Y')
        plt.xlabel('Time (s)')
        fig2.gca().set_ylabel(r'$\sigma_2$')

        fig3, ax3 = plt.subplots(figsize=(6, 4))
        plt.plot(t_vec, X_k[8,1:], label='Y')
        plt.xlabel('Time (s)')
        fig3.gca().set_ylabel(r'$\sigma_3$')

        fig4, ax4 = plt.subplots(figsize=(6, 4))
        plt.plot(t_vec, X_k[0,1:], label='X')
        plt.plot(t_vec, X_k[1,1:], label='Y')
        plt.plot(t_vec, X_k[2,1:], label='Z')
        plt.xlabel('Time (s)')
        plt.ylabel('Position (m)')
        plt.legend()

        return fig1, fig2, fig3, fig4








